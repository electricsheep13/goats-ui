'use strict';

angular.module('goatsUiApp')
	.controller('MenuCtrl', ['$scope', function($scope) {
		
	}])
	.config(function($routeProvider) {
	    $routeProvider
	      .when('/', {
	        templateUrl: 'views/main.html',
	        controller: 'MainCtrl',
	        controllerAs: 'main'
	      })
	      .when('/about', {
	        templateUrl: 'views/about.html',
	        controller: 'AboutCtrl',
	        controllerAs: 'about'
	      })
	      .otherwise({
	        redirectTo: '/'
	      });
	})
	.directive('menu', function() {
		return {
			templateUrl: "views/nav/menu.html"
		}
	});