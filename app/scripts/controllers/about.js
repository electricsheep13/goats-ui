'use strict';

/**
 * @ngdoc function
 * @name goatsUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the goatsUiApp
 */
angular.module('goatsUiApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
