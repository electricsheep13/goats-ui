'use strict';

/**
 * @ngdoc function
 * @name goatsUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the goatsUiApp
 */
angular.module('goatsUiApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
