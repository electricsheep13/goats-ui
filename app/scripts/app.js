'use strict';

/**
 * @ngdoc overview
 * @name goatsUiApp
 * @description
 * # goatsUiApp
 *
 * Main module of the application.
 */
angular
  .module('goatsUiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);
